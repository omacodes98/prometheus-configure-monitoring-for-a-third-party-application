# Prometheus - Configure Monitoring for a Third-Party Application

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

Monitor Redis using Prometheus Exporter 
 - Deploy Redis service in our cluster 
 - Deploy Redis exporter using Helm Chart 
 - Configure Alert Rules(when Redis is down or has too many connections)
 - Import Grafana Dashboard for Redis to visualize monitoring data in  Grafana

## Technologies Used 

* Prometheus 

* Kubernetes 

* Redis 

* Helm 

* Grafana 

## Steps 

Step 1: Create a redis values file because you are going to be using helm to install the exporters so you would need to changes some default values for the file

[Create values file](/images/01%20Create%20a%20redis%20values%20file%20because%20we%20are%20going%20to%20be%20using%20helm%20to%20install%20the%20exporters%20so%20we%20would%20need%20to%20changes%20some%20default%20values%20for%20our%20file.png)

Step 2: in the redis value file the first value to change is serviceMonitor enabled as true this is because serviceMonitor component is the link between the exporter and prometheus application so that prometheus will take  exporter as one of its targets and start scrapping one of its end points for the matrics

[setting serviceMonitor](/images/02%20in%20the%20redis%20value%20file%20the%20first%20value%20to%20change%20is%20serviceMonitor%20enabled%20as%20true%20this%20is%20because%20serviceMonitor%20component%20is%20the%20link%20between%20the%20exporter%20and%20prometheus%20application%20so%20that%20prometheus%20will%20take%20%20exporter%20as%20one%20of%20its%20targets%20.png)

Step 3: Next value to add is labels to allow prometheus match the labels and identify exporter as a new target, set label to release monitoring

[Setting labels release monitoring](/images/03%20Next%20value%20to%20add%20is%20labels%20to%20allow%20prometheus%20match%20the%20labels%20and%20identify%20exporter%20as%20a%20new%20target_%20set%20label%20to%20release%20monitoring.png)

Step 4: Lastly set value of redisAddress

[redisAddress](/images/04%20lastly%20set%20value%20of%20redisAddress.png)

Step 5: Add and update the repository where the chart that has exporter is located

    helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
    help repo update 

[Adding and updating repo](/images/05%20add%20and%20update%20the%20repository%20were%20the%20chart%20that%20has%20exporter%20is%20located.png)

Step 6: install the redis exporter helm chart

    helm install redis-exporter prometheus-community/prometheus-redis-exporter -f redis-values.yaml

[Redis exporter chart installation](/images/06%20install%20the%20redis%20exporter%20helm%20chart.png)

[redis exporter pod](/images/07%20redis%20exporter%20pod%20running%20in%20the%20cluster.png)

[service monitor configuration](/images/08%20service%20monitor%20configurations%20.png)

[redis exporter in prometheus targets](/images/09%20redis%20exporter%20showing%20is%20a%20target%20in%20prometheus.png)

Step 7: Create a file for configurng prometheus rules for redis metrics

[Created file for configuring prometheus rules for redis metrics](/images/10%20create%20a%20file%20for%20configurationprometheus%20rules%20for%20redis%20metrics.png)

Step 8: Set necessary configurations like apiversion, kind to prometheusrule, metadata name to what you want it to be called in cluster and necessary labels that will enable prometheus identify the rule

[Setting configurations](/images/11%20set%20necessary%20configurations%20like%20apiversion%2C%20kind%20to%20prometheusrule%2C%20metadata%20name%20to%20what%20you%20want%20it%20to%20be%20called%20in%20cluster%20and%20necessary%20labels%20that%20will%20enable%20prometheus%20identify%20the%20rule.png)

Step 9: In spec configuration add groups you want it to be in and necessary rules that you want, in my case i wanted a rule that alerts me when redis is down, you can check for syntax and useful alerts in awesome prometheus alerts

[spec configuration](/images/12%20in%20spec%20configuration%20add%20groups%20you%20want%20it%20to%20be%20in%20and%20necessary%20rules%20that%20you%20want_%20in%20my%20case%20i%20wanted%20a%20rule%20that%20alerts%20me%20when%20redis%20is%20down_%20you%20can%20check%20for%20syntax%20and%20useful%20alerts%20in%20awesome%20prometheus%20alerts.png)

Step 10: Set an alert rule for too many connection with logic if redis_connected_clients is greater than 100 then send an alert

[Configure alert rule that will notify you for too much connections](/images/13%20set%20an%20alert%20rool%20for%20too%20many%20connection%20with%20logic%20if%20redis_connected_clients%20is%20greater%20than%20100%20then%20send%20an%20alert.png)


Step 11: Apply configuration file for redis rules 

    kubectl apply -f redis-rules.yaml 

[Apply redis rules configuration file](/images/14%20apply%20configuration%20file%20for%20redis%20rules.png)

[Prometheus rule in cluster](/images/15%20prometheus%20rule%20in%20cluster.png)

Step 12: Edit deployment file for redis by setting the number of replicas to 0 to trigger alert

[deployment file](/images/16%20edit%20deployment%20file%20for%20redis%20by%20setting%20the%20number%20of%20replicas%20to%200%20to%20trigger%20alert.png)

[alert triggered](/images/17%20alert%20triggered.png)

Step 13: Import graphana dashboard for redis, you do this by going on the graghana ui and going to create dashboard and import, in the import section past id of the dashboard

[importing dashboard](/images/18%20Next%20step%20is%20to%20import%20graphana%20dashboard%20for%20redis_%20you%20do%20this%20by%20going%20on%20the%20graghana%20ui%20and%20going%20to%20create%20dashboard%20and%20import_%20in%20the%20import%20section%20past%20id%20of%20the%20dashboard.png)

Step 14: give the dashboard a name and set it to be in general folder and data source to prometheus

[name dashboard](/images/19%20give%20the%20dashboard%20a%20name_%20se%20it%20to%20be%20in%20general%20folder%20and%20data%20source%20to%20prometheus.png)

## Installation

    brew install eksctl 

## Usage 

    kubectl apply -f redis-rules.yaml 

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/prometheus-configure-monitoring-for-a-third-party-application.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review



## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/prometheus-configure-monitoring-for-a-third-party-application

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.